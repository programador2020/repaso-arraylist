package miclase;

import java.util.ArrayList;

public class MiClase {

    public static void main(String[] args) {
        System.out.println("Hola");

        //Declarado
        ArrayList<Productos> listado;
        //Inicializar
        listado = new ArrayList();

        Productos miProducto1 = new Productos();
        miProducto1.nombre = "Guitarra";
        miProducto1.precio = 12000;
        miProducto1.descuento = 2400;

        // System.out.println(miProducto1);
        Productos miProducto2 = new Productos();
        miProducto2.nombre = "Guitarra Fender";
        miProducto2.precio = 100000;
        miProducto2.descuento = 0;

        //  System.out.print(miProducto2);
        Productos miProducto3 = new Productos();
        miProducto3.nombre = "Gibson Les Paul";
        miProducto3.precio = 500000;
        miProducto3.descuento = 50000;

        //  System.out.println(miProducto3);
        listado.add(miProducto1);
        listado.add(miProducto2);
        listado.add(miProducto3);

        System.out.println(listado);

        try {
            //Recorremos el listado
            for (int i = 0; i < 3; i++) {
                System.out.println(listado.get(i).nombre);
            }
        } catch (Exception miExepcion) {
            System.out.println("Hubo un inconveniente.");
        }finally{
            //Este bloque nos sirve para cerrar la conexion con la
            //base de datos
        }
        
        //1.
        //Declaramos
        String nombre;
        //Inicializamos
        nombre = "";
        
        
        //2.
        //Declaramos e inicializamos
        String apellido = "Lopez";
        
        
        System.out.println("Nombre: " + nombre);
        System.out.println("Apellido: " + apellido);
        System.out.println("Estado de nombre: " + nombre.isEmpty());
        
        System.out.println("Chau");

    }

}
